FROM python:3.10

ARG HTTPS_PROXY
ARG HTTP_PROXY

ARG https_proxy=$HTTPS_PROXY
ARG http_proxy=$HTTP_PROXY

RUN echo "Acquire::http::Proxy \""$HTTP_PROXY"\";" >> /etc/apt/apt.conf \
    && echo "Acquire::https::Proxy \""$HTTPS_PROXY"\";" >> /etc/apt/apt.conf \
    && export https_proxy=$HTTPS_PROXY \
    && export http_proxy=$HTTP_PROXY

RUN apt update -y   \
    && apt install -y --no-install-recommends \
    wget \
    && rm -f /etc/apt/apt.conf

RUN useradd seta 

ARG ROOT=/home/seta

WORKDIR $ROOT
COPY ./requirements.txt ./requirements.txt

RUN pip install --no-cache-dir -r requirements.txt 

#copy configuration file
COPY ./config/data.conf /etc/seta/

COPY ./src ./
RUN mkdir $ROOT/models_docker
COPY ./models ./models_docker

WORKDIR $ROOT/models_docker

RUN wget https://code.europa.eu/seta/models/word2vec/-/raw/main/seta_suggestions-v1.0.0.json.bz2 
RUN   bzip2 -d  seta_suggestions-v1.0.0.json.bz2 \
    && mv  seta_suggestions-v1.0.0.json json_suggestion.json  \
    && sha256sum -b -z json_suggestion.json  | cut -d " " -f1 > json_suggestion.json.crc 

RUN   sha256sum -b -z suggestion-data-mapping.json  | cut -d " " -f1 > suggestion-data-mapping.crc 
RUN   sha256sum -b -z taxonomy-data-mapping.json  | cut -d " " -f1 > taxonomy-data-mapping.crc 
RUN   sha256sum -b -z data-mapping.json  | cut -d " " -f1 > data-mapping.crc 


CMD ["python3", "/home/seta/app.py"]
